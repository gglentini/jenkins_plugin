package org.jenkinsci.plugins.platinum;

import jenkins.model.Jenkins;
import hudson.DescriptorExtensionList;
import hudson.model.AbstractDescribableImpl;
import hudson.model.BuildListener;
import hudson.model.AbstractBuild;
import hudson.model.Descriptor;

public abstract class PlatinumNotificationProvider extends AbstractDescribableImpl<PlatinumNotificationProvider> {
	public abstract String getNotification(AbstractBuild<?, ?> build, BuildListener listener);

	  public static DescriptorExtensionList<PlatinumNotificationProvider, Descriptor<PlatinumNotificationProvider>> all() {
	    return Jenkins.getInstance().getDescriptorList(PlatinumNotificationProvider.class);
	  }
}
