package org.jenkinsci.plugins.platinum;

import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;
import static javax.servlet.http.HttpServletResponse.SC_OK;
import hudson.Extension;
import hudson.model.UnprotectedRootAction;
import hudson.model.AbstractModelObject;
import hudson.model.AbstractProject;
import hudson.model.Hudson;
import hudson.plugins.mercurial.MercurialSCM;
import hudson.scm.SCM;
import hudson.security.ACL;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;

import org.acegisecurity.context.SecurityContext;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.commons.lang.StringUtils;
import org.kohsuke.stapler.HttpResponse;
import org.kohsuke.stapler.HttpResponses;
import org.kohsuke.stapler.QueryParameter;
import org.kohsuke.stapler.StaplerRequest;
import org.kohsuke.stapler.StaplerResponse;

import com.google.common.collect.Lists;


/**
 * Information screen for the use of Platinum in Jenkins.
 */
@Extension
public class PlatinumStatus extends AbstractModelObject implements UnprotectedRootAction {
  private static final Logger LOGGER = Logger.getLogger(PlatinumStatus.class.getName());

  public HttpResponse doNotifyPullRequest(@QueryParameter(required = true) String url, @QueryParameter(required = false) Long id) throws ServletException,
      IOException {
    // run in high privilege to see all the projects anonymous users don't see.
    // this is safe because when we actually schedule a build, it's a build that can
    // happen at some random time anyway.
    SecurityContext old = ACL.impersonate(ACL.SYSTEM);
    try {
        return handleNotifyPullRequest(new URI(url), id);
    } catch ( URISyntaxException ex ) {
        throw HttpResponses.error(SC_BAD_REQUEST, ex);
    } finally {
        SecurityContextHolder.setContext(old);
    }
  }

  private HttpResponse handleNotifyPullRequest(URI uri, Long id) throws ServletException, IOException {
	  try {

		  final List<AbstractProject<?,?>> projects = Lists.newArrayList();
		  boolean scmFound = false,
				  triggerFound = false,
				  urlFound = false;
		  for (AbstractProject<?, ?> project : Hudson.getInstance().getAllItems(AbstractProject.class)) {
			  SCM scm = project.getScm();
			  if (!(scm instanceof MercurialSCM)) {
				  continue;
			  }
			  scmFound = true;

			  PlatinumPullRequestTrigger trigger = project.getTrigger(PlatinumPullRequestTrigger.class);

			  if (trigger == null) {
				  continue;
			  }
			  triggerFound = true;

			  URI triggerUri = null;
			  try {
				  triggerUri = new URI(trigger.getPlatinumURL());
			  } catch (URISyntaxException e1) {
				  e1.printStackTrace();
			  }
			  if (!looselyMatches(uri, triggerUri)) {
				  continue;
			  }

			  urlFound = true;

			  LOGGER.log(Level.INFO, "Triggering the polling of {0}", project.getFullDisplayName());

			  if (!project.isDisabled()) {
				  try {
					  LOGGER.finer("Triggering of " + project.getFullDisplayName());
					  trigger.buildPullRequest(id);
					  projects.add(project);
					  LOGGER.finer("Triggered " + project.getFullDisplayName());
				  } catch (ExecutionException e) {
					  LOGGER.warning("Couldn't trigger " + project.getFullDisplayName());
				  } catch (TimeoutException e) {
					  LOGGER.warning("Couldn't trigger " + project.getFullDisplayName());
				  } catch (URISyntaxException e) {
					  LOGGER.warning("Couldn't trigger " + project.getFullDisplayName());
				  }
			  }
		  }
		  final StringBuilder msgBuilder = new StringBuilder();
		  if (!scmFound) {
			  msgBuilder.append("No Mercurial jobs found");
		  } else if (!urlFound) {
			  msgBuilder.append("No Mercurial jobs associated to Platinum branch: " + uri);
		  } else if (!triggerFound) {
			  msgBuilder.append("Jobs found but they aren't configured for polling");
		  }

		  return new HttpResponse() {
			  public void generateResponse(StaplerRequest req, StaplerResponse rsp, Object node) throws IOException, ServletException {
				  rsp.setStatus(SC_OK);
				  rsp.setContentType("text/plain");
				  for (AbstractProject<?, ?> p : projects) {
					  rsp.addHeader("Triggered", p.getAbsoluteUrl());
				  }
				  PrintWriter w = rsp.getWriter();
				  for (AbstractProject<?, ?> p : projects) {
					  w.println("Triggered " + p.getFullDisplayName());
				  }
				  w.println(msgBuilder.toString());
			  }
		  };
  } catch (InterruptedException e) {
      throw new ServletException(e);
    }
	  
 }
  
  /**
   * Used to test if what we have in the job configuration matches what was submitted to the notification endpoint.
   * It is better to match loosely and wastes a few polling calls than to be pedantic and miss the push notification,
   * especially given that Git/Mercurial tend to support multiple access protocols.
   */
  protected boolean looselyMatches(URI lhs, URI rhs) {
    return StringUtils.equals(lhs.getHost(), rhs.getHost()) && StringUtils.equals((lhs.getPath()), (rhs.getPath()));
  }

public String getDisplayName() {
	return "Platinum";
}

public String getIconFileName() {
	return null;
}

public String getSearchUrl() {
	return getUrlName();
}

public String getUrlName() {
	return "platinum";
}
}
