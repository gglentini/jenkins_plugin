package org.jenkinsci.plugins.platinum;

import java.text.MessageFormat;
import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import jenkins.plugins.asynchttpclient.AHC;

import com.google.gson.Gson;
import com.ning.http.client.AsyncHttpClient;
import com.ning.http.client.AsyncHttpClient.BoundRequestBuilder;
import com.ning.http.client.Response;

import net.amadeus.httpServices.core.json.HTTPJsonResponseAdapter;
import net.amadeus.httpServices.core.HTTPJsonResponse;


/**
 * Java Platinum Client (a very simple one, based on JSON API)
 */
public class PlatinumClient {

	private final static Logger LOGGER = Logger.getLogger(PlatinumClient.class.getName());

	public static enum BuildState {
		testdoneko, testongoing, testdoneok, testnotstarted
	}

	/**
	 * Timeout in milliseconds for Stash requests.
	 */
	private static int timeoutMs = 1000;

	/**
	 * Platinum root url.
	 */
	private String              url;
	private final static String PLATINUM_API = "/api/json";

	public PlatinumClient(String url) {
		this.url = url;
	}

	private BoundRequestBuilder prepareGet(String request) {
		LOGGER.finer("HTTP GET " + request);
		return AHC.instance().prepareGet(request);
	}


	private BoundRequestBuilder preparePost(String request) {
		LOGGER.finer("HTTP POST " + request);
		return AHC.instance().preparePost(request);
	}


	private Response executeRequest(BoundRequestBuilder requestBuilder) throws InterruptedException, IOException, ExecutionException, TimeoutException {
		//		    addAuthorization(requestBuilder);
		return requestBuilder.execute().get(timeoutMs, TimeUnit.MILLISECONDS);
	}

	public PlatinumPullRequest getPullRequest(String instance, Long id) throws IOException, InterruptedException, ExecutionException, TimeoutException {
		String request = MessageFormat.format("{0}/pullrequest/{1}/", getPlatinumApi(instance), id.toString());
		BoundRequestBuilder requestBuilder = prepareGet(request);
		Response response;
		response = executeRequest(requestBuilder);
		if (response.getStatusCode() == 200) {
			//TODO: we need to check the returned 'stat' value here, and if equals to "ok",
			//parse the 'result' then to a PullRequest object
			//		    	Gson gson = new Gson();
			//		    	return gson.fromJson(response.getResponseBody(), PlatinumPullRequest.class);
			HTTPJsonResponse jsonResponse = HTTPJsonResponseAdapter.parse(response.getResponseBody());
			return HTTPJsonResponseAdapter.getGson().fromJson(jsonResponse.getResult(), PlatinumPullRequest.class);
		}
		return null;
	}
	
	public ErrorResponse changePullRequestStatus(String instance, String branch, Long id, BuildState status) throws InterruptedException, IOException {
		// http://platinum/dum/api/json/pendingstatus/1/(testongoing|testdoneok|testdoneko|testnotstarted)/156/
		String request = MessageFormat.format("{0}/pendingstatus/{1}/{2}/{3}/", getPlatinumApi(instance), branch, status, id.toString());
		BoundRequestBuilder requestBuilder = preparePost(request);
		requestBuilder.addQueryParameter("status_id", "continuousbuildstatus");
		Response response;
		
	    ErrorResponse res=null;
		try {
	        response = executeRequest(requestBuilder);
	        if (response.getStatusCode() != 200) {
	        //TODO error handling...but the server always sends 200 even if we got an error,
	        // so we should better catch exceptions here
	        }
	      } catch (ExecutionException e) {
	        res = new ErrorResponse(new Error(e));
	      } catch (TimeoutException e) {
	        res = new ErrorResponse(new Error(e));
	      } 
		
		return res;
	}

	private Object getPlatinumApi(String instance) {
		return url + instance + PLATINUM_API;
	}
	
	
}
