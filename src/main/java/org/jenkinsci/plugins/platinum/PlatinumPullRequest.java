package org.jenkinsci.plugins.platinum;

import java.io.File;

//import net.amadeus.jface.string.StringTools;

public class PlatinumPullRequest {

	private String comment;
	private String instance;
	private int creation_branch=-1;
	private WinaproachRecord[] record_list;
	private String repository;
	private String full_bundle_path;
	//private RepositoryMountInfo repository_mount_info;
	private String parent_revision;
	private int id;
	private String review_id;
	private PlatinumUser developer;
	private String title;
	private String save_date;
	private String code_review_revision;
	private String review_link;
	//private PullRequestUrgency urgency;
	private String revision;
	//private PullRequestStatus status;
	private String pending_status;
	private String pending_status_label;
	private String pending_status_color;
	private String webrepository;
	private String bundle_name;
	private boolean is_bundle;
	private String internal_diff;
	private String root_path;
	private String diff_name;
	private String webdiff;
	private int parent_pull;
	private String[] flags;
	//private InterfaceChange interface_change;
	

	public PlatinumPullRequest() {//Added for JSON parser
	}
	
	
	public String[] getFlags() {
		return flags==null?null:flags.clone();
	}


	public void setFlags(String[] flags) {
		this.flags = flags==null?null:flags.clone();
	}


	public int getParent_pull() {
		return parent_pull;
	}


	public void setParent_pull(int parent_pull) {
		this.parent_pull = parent_pull;
	}


	public String getInstance() {
		return instance;
	}


	public void setInstance(String instance) {
		this.instance = instance;
	}

	//TODO to remove, just a patch because server is not sending yet interface change value
//	public InterfaceChange getInterface_change() {
//		return interface_change==null?InterfaceChange.no:interface_change;
//	}


//	public void setInterface_change(InterfaceChange interfaceChange) {
//		interface_change = interfaceChange;
//	}


	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}
	/**
	 * @param comment the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}
	/**
	 * @return the creation_branch
	 */
	public int getCreation_branch() {
		return creation_branch;
	}
	/**
	 * @param creationBranch the creation_branch to set
	 */
	public void setCreation_branch(int creationBranch) {
		creation_branch = creationBranch;
	}
	/**
	 * @return the record_list
	 */
	public WinaproachRecord[] getRecord_list() {
		return record_list==null?null:record_list.clone();
	}
	/**
	 * @param recordList the record_list to set
	 */
	public void setRecord_list(WinaproachRecord[] recordList) {
		record_list = record_list==null?null:record_list.clone();
	}
	/**
	 * @return the repository
	 */
	public String getRepository() {
		return repository;
	}
	/**
	 * @param repository the repository to set
	 */
	public void setRepository(String repository) {
		this.repository = repository;
	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the review_id
	 */
	public String getReview_id() {
		return review_id;
	}
	/**
	 * @param reviewId the review_id to set
	 */
	public void setReview_id(String reviewId) {
		review_id = reviewId;
	}
	/**
	 * @return the developer
	 */
	public PlatinumUser getDeveloper() {
		return developer;
	}
	/**
	 * @param developer the developer to set
	 */
	public void setDeveloper(PlatinumUser developer) {
		this.developer = developer;
	}
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @return the save_date
	 */
	public String getSave_date() {
		return save_date;
	}
	/**
	 * @param saveDate the save_date to set
	 */
	public void setSave_date(String saveDate) {
		save_date = saveDate;
	}
	/**
	 * @return the review_link
	 */
	public String getReview_link() {
		return review_link;
	}
	/**
	 * @param reviewLink the review_link to set
	 */
	public void setReview_link(String reviewLink) {
		review_link = reviewLink;
	}
	/**
	 * @return the urgency
	 */
//	public PullRequestUrgency getUrgency() {
//		return urgency;
//	}
	/**
	 * @param urgency the urgency to set
	 */
//	public void setUrgency(PullRequestUrgency urgency) {
//		this.urgency = urgency;
//	}
	/**
	 * @return the revision
	 */
	public String getRevision() {
		return revision;
	}
	/**
	 * @param revision the revision to set
	 */
	public void setRevision(String revision) {
		this.revision = revision;
	}
	public String getParent_revision() {
		return parent_revision;
	}
	public void setParent_revision(String parentRevision) {
		parent_revision = parentRevision;
	}
	public boolean canBeCreated() {
		return creation_branch>=0;
	}
	public String getCode_review_revision() {
		return code_review_revision;
	}
	public void setCode_review_revision(String codeReviewRevision) {
		code_review_revision = codeReviewRevision;
	}
//	public PullRequestStatus getStatus() {
//		return status;
//	}
//	public void setStatus(PullRequestStatus status) {
//		this.status = status;
//	}
	public String getPending_status() {
		return pending_status;
	}
	public void setPending_status(String pendingStatus) {
		pending_status = pendingStatus;
	}
	public String getPending_status_label() {
		return pending_status_label;
	}
	public void setPending_status_label(String pendingStatusLabel) {
		pending_status_label = pendingStatusLabel;
	}
	public String getPending_status_color() {
		return pending_status_color;
	}
	public void setPending_status_color(String pendingStatusColor) {
		pending_status_color = pendingStatusColor;
	}
	public boolean isIntegrable() {
		return repository!=null&&new File(repository).exists();
	}
//	public RepositoryMountInfo getRepository_mount_info() {
//		return repository_mount_info;
//	}
//	public void setRepository_mount_info(RepositoryMountInfo repositoryMountInfo) {
//		repository_mount_info = repositoryMountInfo;
//	}
	public String getInternal_diff() {
		return internal_diff;
	}
	public void setInternal_diff(String internal_diff) {
		this.internal_diff = internal_diff;
	}
	public String getRoot_path() {
		return root_path;
	}
	public void setRoot_path(String root_path) {
		this.root_path = root_path;
	}
	public void setWebrepository(String webrepository) {
		this.webrepository = webrepository;
	}
	public String getWebrepository() {
		return webrepository;
	}
	public String getBundle_name() {
		return bundle_name;
	}
	public void setBundle_name(String bundleName) {
		bundle_name = bundleName;
	}
	public String getDiff_name() {
		return diff_name;
	}
	public void setDiff_name(String diffName) {
		diff_name = diffName;
	}
	public String getWebdiff() {
		return webdiff;
	}
	public void setWebdiff(String webdiff) {
		this.webdiff = webdiff;
	}
	public String getFull_bundle_path() {
		return full_bundle_path;
	}
	public void setFull_bundle_path(String full_bundle_path) {
		this.full_bundle_path = full_bundle_path;
	}
	public boolean containChangeSet() {
		return parent_revision!=null&&revision!=null&&!parent_revision.equals(revision);
	}
//	public String getRecordsAsOneString() {
//		return StringTools.join(record_list, ", ",Record.RECORD_ID_STRING);
//	}
//	public String[] getPathRepository() {
//		return repository_mount_info==null?new String[]{repository}:new String[]{repository,repository_mount_info.getPath()};
//	}
	public boolean isIs_bundle() {
		return is_bundle;
	}
	public void setIs_bundle(boolean is_bundle) {
		this.is_bundle = is_bundle;
	}
//	public boolean isCodeReviewCreated(){
//		return !StringTools.isEmpty(getReview_id());
//	}
}
