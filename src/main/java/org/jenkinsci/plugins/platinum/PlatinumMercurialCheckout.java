package org.jenkinsci.plugins.platinum;

import hudson.AbortException;
import hudson.Extension;
import hudson.FilePath;
import hudson.model.BuildListener;
import hudson.model.AbstractBuild;
import hudson.model.AbstractProject;
import hudson.model.Cause;
import hudson.model.Computer;
import hudson.model.Node;
import hudson.plugins.mercurial.HgExe;
import hudson.plugins.mercurial.MercurialSCM;

import java.io.IOException;

import jenkins.scm.SCMCheckoutStrategyDescriptor;
import jenkins.scm.SCMCheckoutStrategy;

import org.kohsuke.stapler.DataBoundConstructor;


public class PlatinumMercurialCheckout extends SCMCheckoutStrategy {
  @DataBoundConstructor
  public PlatinumMercurialCheckout() {

  }
  @Extension
  public static class DescriptorImpl extends SCMCheckoutStrategyDescriptor {

    @Override
    public boolean isApplicable(AbstractProject project) {
      return true;
    }

    @Override
    public String getDisplayName() {
      return "Platinum Mercurial strategy";
    }

  }

	@Override
	public void checkout(AbstractBuild.AbstractBuildExecution execution)
            throws IOException,
                   InterruptedException {
    //execution.getListener().getLogger().println("Platinum is here!");
    execution.defaultCheckout();
    fetch_pullrequest(execution);
    
    //TODO if fetchpt fails, the PR can't be integrated. Check if a rollback is launched in this case.

    //TODO new changelog taking into account the PR if successfully integrated in the workspace
    
    //TODO after each build, it doesn't matter the result, the workspace is currently
    // not cleaned. What we should here?
    // Our idea is to do a 'clean' build, using Jenkins Mercurial plugin parameter to specify this behaviour.
	}
	
	@SuppressWarnings("deprecation")
	private void fetch_pullrequest(AbstractBuild.AbstractBuildExecution execution)
    		throws IOException, InterruptedException {
    	
		// getting info needed to call Mercurial plugin HG wrapper
		BuildListener listener = execution.getListener();
		AbstractBuild<?, ?> build = (AbstractBuild<?, ?>) execution.getBuild();
		AbstractProject<?, ?> project = (AbstractProject<?, ?>) execution.getProject();
		MercurialSCM scm = (MercurialSCM) project.getScm();
    	HgExe hg = new HgExe(scm, execution.getLauncher(), build, listener);
        
        FilePath repository = project.getWorkspace();
        
        // get the cause of the current job, to get the Platinum parameters
        PlatinumPullRequestCause cause = build.getCause(PlatinumPullRequestCause.class);
        
        int fetchExitCode;
        
        try {
            // fetch the pull request specified in the cause and in the trigger
        	// using Platinum Mercurial extension, currently 100% working on Linux
        	// To be tested, if needed, in Windows environment
        	fetchExitCode = hg.run("fetchpt", 
        			"--instance=http://"+cause.getPlatinumHost()+"/"+cause.getPlatinumInstance(), 
        			"--product", cause.getPlatinumProduct(), 
        			"--branch", cause.getPlatinumBranch(), 
        			"--pullrequest", String.valueOf(cause.getPullRequestId())).pwd(repository).join();
        } catch (IOException e) {
//            if (causedByMissingHg(e)) {
//                listener.error("Failed to fetch the pull request because hg could not be found;" +
//                        " check that you've properly configured your Mercurial installation");
//            } else {
//                e.printStackTrace(listener.error("Failed to fetch the pull request"));
//            }
            throw new AbortException("Failed to fetch the pull request");
        }
        
        if (fetchExitCode !=0 ) {
        	listener.error("Failed to fetch the pull request");
    		throw new AbortException("Failed to fetch the pull request");
        }
    }

}
