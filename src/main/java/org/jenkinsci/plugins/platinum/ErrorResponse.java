package org.jenkinsci.plugins.platinum;

import java.util.Arrays;

public class ErrorResponse {

  private Error[] errors;

  public ErrorResponse() {
  }

  public ErrorResponse(Error... errors) {
    this.errors = errors;
  }

  public Error[] getErrors() {
    return errors;
  }

  public void setErrors(Error[] errors) {
    this.errors = errors;
  }

  @Override
  public String toString() {
    return "AddCommentResponse [errors=" + Arrays.toString(errors) + "]";
  }
}