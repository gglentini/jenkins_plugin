package org.jenkinsci.plugins.platinum;

import hudson.Extension;
import hudson.model.Item;
import hudson.model.ParameterValue;
import hudson.model.SCMedItem;
import hudson.model.AbstractProject;
import hudson.model.ParameterDefinition;
import hudson.model.ParametersAction;
import hudson.model.ParametersDefinitionProperty;
import hudson.model.StringParameterValue;
import hudson.triggers.Trigger;
import hudson.triggers.TriggerDescriptor;
import hudson.util.FormValidation;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;

import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;

import org.kohsuke.stapler.DataBoundConstructor;
import org.kohsuke.stapler.QueryParameter;
import org.kohsuke.stapler.StaplerRequest;

import antlr.ANTLRException;



public class PlatinumPullRequestTrigger extends Trigger<SCMedItem> {
	  @Extension
	  public static class DescriptorImpl extends TriggerDescriptor {

	    @Override
	    public boolean configure(StaplerRequest req, JSONObject json) throws FormException {
	      req.bindJSON(this, json);
	      return true;
	    }

	    @Override
	    public String getDisplayName() {
	      return "Platinum Pull Request Trigger";
	    }

	    @Override
	    public boolean isApplicable(Item item) {
	      return item instanceof AbstractProject;
	    }

	  }
	
	private static final Logger LOGGER = Logger.getLogger(PlatinumStatus.class.getName());
	
	//private static final PlatinumClient platinumInstance = new PlatinumClient("http://platinum/");
	
	public static final String PARAMETER_PR_ID         		= "PLATINUM_PR_ID";
	public static final String PARAMETER_PLATINUM_INSTANCE 	= "PLATINUM_INSTANCE";
	public static final String PARAMETER_PLATINUM_PRODUCT	= "PLATINUM_PRODUCT";
	public static final String PARAMETER_PLATINUM_BRANCH	= "PLATINUM_BRANCH";
	public static final String PARAMETER_PLATINUM_HOST		= "PLATINUM_HOST";
	
	private final String platinumURL;
	private String platinum_instance;
	private String platinum_product;
	private String platinum_branch;
	private String platinum_host;
	
	@DataBoundConstructor
	public PlatinumPullRequestTrigger(String spec, String platinumUrl) throws ANTLRException, MalformedURLException {
		super(StringUtils.defaultIfBlank(spec, "@yearly"));
		this.platinumURL = platinumUrl;
		
	}
	
	public FormValidation doCheckPlatinumUrl(@QueryParameter String value) {
		//TODO validation not called...
		//for the moment, trigger URL with trailing slash ONLY are properly working
		  if(looksOk(value))  return FormValidation.ok();
		  else                return FormValidation.error("No valid URL provided.");
		}
	
	private boolean looksOk(String value) {
		try {
			  @SuppressWarnings("unused")
			URI triggerUri = new URI(value);
		  } catch (URISyntaxException e1) {
			  // TODO Auto-generated catch block
				return false;
		  }
		if (!StringUtils.endsWith(value, "/")) {
			value += "/";
		}
		return true;
	}

	public String getPlatinumURL() {
		return platinumURL;
	}
	
	public String getPlatinumInstance() {
		return platinum_instance;
	}
	
	public String getPlatinumProduct() {
		return platinum_product;
	}
	
	public String getPlatinumBranch() {
		return platinum_branch;
	}
	
	public void buildPullRequest(Long id) 
			throws IOException, InterruptedException, ExecutionException, TimeoutException, URISyntaxException, MalformedURLException {
		if (id == null) {
		      return;
		    }
	
		// this should go in the constructor, but currently id doesn't work there...
		URI ptURL = new URI(platinumURL);
		String[] ptInfo = ptURL.getPath().split("/");
		this.platinum_instance = ptInfo[1];
		this.platinum_product = ptInfo[2];
		this.platinum_branch = ptInfo[3];
		this.platinum_host = ptURL.getAuthority();
		
		//TODO platinum host string could be built in a cleaner way...
		PlatinumClient platinumInstance = new PlatinumClient("http://"+this.platinum_host+"/");
		PlatinumPullRequest pullRequest = platinumInstance.getPullRequest(platinum_instance, id);
		if (pullRequest == null) {
			//TODO: read the jenkins's project Platinum URI here o the URI sent in the request?
			LOGGER.log(Level.WARNING, "Unable to find PR #" + id + " for Platinum branch " + platinumURL);
			return;
		}
		buildPullRequest(pullRequest);
	}
	
	public void buildPullRequest(PlatinumPullRequest pullRequest) {
		
		Long id = new Long(pullRequest.getId());
		AbstractProject<?, ?> project = job.asProject();
		
		LOGGER.log(Level.INFO, "Bulding PR #" + id);
					
		PlatinumPullRequestCause cause = new PlatinumPullRequestCause(pullRequest.getId(), platinum_instance,
																		platinum_product, platinum_branch, platinum_host);
		
		List<ParameterValue> values = getDefaultParameters(project);
	    values.add(new StringParameterValue(PARAMETER_PR_ID, id.toString()));
	    
	    // TODO use the ones taken from the URL set in jenkins or the one retrieved from the PR?
	    values.add(new StringParameterValue(PARAMETER_PLATINUM_INSTANCE, platinum_instance));
	    values.add(new StringParameterValue(PARAMETER_PLATINUM_PRODUCT, platinum_product));
	    values.add(new StringParameterValue(PARAMETER_PLATINUM_BRANCH, platinum_branch));
	    values.add(new StringParameterValue(PARAMETER_PLATINUM_HOST, platinum_host));
	    
	    //TODO create Platinum actions here
//	    StashBadgeAction badgeAction = new StashBadgeAction(id, getStashUrl() + StringUtils.stripStart(url, "/"));
//	    List<Action> actions = Arrays.asList(new ParametersAction(values));
	    
	    project.scheduleBuild2(0, cause, new ParametersAction(values));
		
	}
	
	private List<ParameterValue> getDefaultParameters(AbstractProject<?, ?> project) {
		    List<ParameterValue> values = new ArrayList<ParameterValue>();
		    ParametersDefinitionProperty definitionProperty = project.getProperty(ParametersDefinitionProperty.class);
		    if (definitionProperty != null) {
		      for (ParameterDefinition definition : definitionProperty.getParameterDefinitions()) {
		        values.add(definition.getDefaultParameterValue());
		      }
		    }

		    return values;
		  }
}
