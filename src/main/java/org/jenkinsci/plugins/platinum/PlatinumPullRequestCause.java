package org.jenkinsci.plugins.platinum;

import hudson.model.Cause;

import java.text.MessageFormat;

import org.kohsuke.stapler.export.Exported;

	/**
	 * A cause indicating that the Platinum Pull Request Trigger has started the build. It includes information about the pull
	 * request.
	 * 
	 */
	public class PlatinumPullRequestCause extends Cause {
		
	  private final int pullRequestId;
	  private final String platinumInstance;
	  private final String platinumProduct;
	  private final String platinumBranch;
	  private final String platinumHost;

	  /**
	   * @param pullRequestId
	   */
	  public PlatinumPullRequestCause(int pullRequestId, String platinumInstance, String platinumProduct, String platinumBranch,
			  String platinumHost) {
	    super();
	    this.pullRequestId = pullRequestId;
	    this.platinumInstance = platinumInstance;
	    this.platinumProduct = platinumProduct;
	    this.platinumBranch = platinumBranch;
	    this.platinumHost = platinumHost;
	  }

	  public int getPullRequestId() {
	    return pullRequestId;
	  }
	  
	  public String getPlatinumInstance() {
		  return platinumInstance;
	  }
	  
	  public String getPlatinumProduct() {
		  return platinumProduct;
	  }
	  
	  public String getPlatinumBranch() {
		  return platinumBranch;
	  }
	  
	  public String getPlatinumURL() {
		  return "http://"+platinumHost+"/"+platinumInstance+"/"+platinumProduct+"/"+platinumBranch+"/";
	  }

	  @Exported(visibility = 3)
	  public String getShortDescription() {
		  //TODO put hyperlinks here?
	    return MessageFormat.format("Platinum Pull Request #{0} on {1}", String.valueOf(pullRequestId), getPlatinumURL());
	  }

	public String getPlatinumHost() {
		return platinumHost;
	}

	}

