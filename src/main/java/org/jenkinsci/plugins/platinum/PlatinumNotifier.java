package org.jenkinsci.plugins.platinum;

import java.io.IOException;

import org.jenkinsci.plugins.platinum.PlatinumClient.BuildState;
import org.kohsuke.stapler.DataBoundConstructor;

import hudson.Extension;
import hudson.Launcher;
import hudson.init.InitMilestone;
import hudson.init.Initializer;
import hudson.model.BuildListener;
import hudson.model.Items;
import hudson.model.Result;
import hudson.model.AbstractBuild;
import hudson.model.AbstractProject;
import hudson.tasks.BuildStepDescriptor;
import hudson.tasks.BuildStepMonitor;
import hudson.tasks.Notifier;
import hudson.tasks.Publisher;

public class PlatinumNotifier extends Notifier {
	@Extension
	public static class DescriptorImpl extends BuildStepDescriptor<Publisher> {
		@Initializer(before = InitMilestone.PLUGINS_STARTED)
		public static void addAliases() {
			Items.XSTREAM2.addCompatibilityAlias("org.jenkinsci.plugins.stash.PlatinumPullRequestNotifier", PlatinumNotifier.class);
		}

		@Override
		public String getDisplayName() {
			return "Notify Platinum";
		}

		@SuppressWarnings("rawtypes")
		@Override
		public boolean isApplicable(Class<? extends AbstractProject> jobType) {
			return true;
		}

	}

	private String name;
	private Result threshold = Result.SUCCESS;
	private PlatinumNotificationProvider notificationProvider;

	public BuildStepMonitor getRequiredMonitorService() {
		// TODO Auto-generated method stub
		return BuildStepMonitor.NONE;
	}
	
	@DataBoundConstructor
	public PlatinumNotifier(Result threshold, String name, PlatinumNotificationProvider notificationProvider) {
		super();
		if (threshold != null) {
			this.threshold = threshold;
		}
		this.name = name;
		this.notificationProvider = notificationProvider;
	}

	public String getName() {
		return name;
	}

	public PlatinumNotificationProvider getNotificationProvider() {
		return notificationProvider;
	}
	
	  @Override
	  public boolean perform(AbstractBuild<?, ?> build, Launcher launcher, BuildListener listener) throws InterruptedException, IOException {
	    try {
	    if (threshold.isWorseOrEqualTo(build.getResult())) {
	      setBuildStatus(build, listener, BuildState.testdoneok);
	    } else {
	      setBuildStatus(build, listener, BuildState.testdoneko);
	    }
	    } catch (IOException e) {
	      // don't fail the build if Platinum doesn't answer
	      handleNonFatalException(listener, e);
	    }
	    return true;
	  }

	private void handleNonFatalException(BuildListener listener, IOException e) {
	    listener.getLogger().println("Unable to notify Platinum");
	    e.printStackTrace(listener.getLogger());
	}

	private void setBuildStatus(AbstractBuild<?, ?> build, BuildListener listener, BuildState status) throws InterruptedException, IOException {
		listener.getLogger().println("Notifying the result to Platinum..");
		PlatinumPullRequestCause cause = build.getCause(PlatinumPullRequestCause.class);
		
		//TODO platinum host string could be built in a cleaner way...
		PlatinumClient platinumClient = new PlatinumClient("http://"+cause.getPlatinumHost()+"/");
		platinumClient.changePullRequestStatus(cause.getPlatinumInstance(), cause.getPlatinumBranch(), new Long(cause.getPullRequestId()), status);
	}
}
