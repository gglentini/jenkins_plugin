package org.jenkinsci.plugins.platinum;

//import net.amadeus.jface.string.StringBasedSerializer;


public class WinaproachRecord {
	private String type;
	private int aproach_id;
	private int id;
	private String title;
	
//	public final static StringBasedSerializer<Record> RECORD_ID_STRING=new StringBasedSerializer<Record>(){
//		@Override
//		public String getStringFromObject(Record inputObject) {
//			return String.valueOf(inputObject.getAproach_id());
//		}};
	
	public WinaproachRecord() {
		super();
	}

	@Override
	public boolean equals(Object obj) {
		if(obj instanceof WinaproachRecord){
			return aproach_id==((WinaproachRecord)obj).aproach_id;
		}
		return super.equals(obj);
	}
	@Override
	public int hashCode() {
		return aproach_id;
	}
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	/**
	 * @return the aproach_id
	 */
	public int getAproach_id() {
		return aproach_id;
	}
	/**
	 * @param aproachId the aproach_id to set
	 */
	public void setAproach_id(int aproachId) {
		aproach_id = aproachId;
	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
}
