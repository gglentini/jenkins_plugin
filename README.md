
#Platinum Jenkins Plugin

## Overview
This Jenkins plugin provides integration with [Platinum](http://platinum.nce.amadeus.net/).

## Installation
TODO

## Usage
### Build Merge result of Pull Requests
1. Create a job configured to target a repository hosted on a Platinum instance using the Mercurial plugin.
2. In *Source Code Management* section, enter the Mercurial repository URL
3. In *Advanced Source Code Management*, select **Platinum Mercurial strategy** as checkout strategy.
7. In *Build Triggers*, enable **Platinum Pull Request Trigger**, and set the URL of the Platinum branch you want to track

### Notify Platinum of a build result.
1. In any job that needs to notify Platinum, add the post-build action named **Notify Platinum**.
2. The plugin will change the Continuous Build (CB) pull request's'status accordingly to the build state.
